#ifndef _UTIL_H_
#define _UTIL_H_

#include "mem_internals.h"

#include <stddef.h>
#include <stdio.h>



inline size_t size_max( size_t x, size_t y ) { return (x >= y)? x : y ; }

_Noreturn void err( const char* msg, ... );
static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}


#endif
