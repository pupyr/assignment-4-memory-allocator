#include <stdbool.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

bool test1(){
    size_t heapSize=getpagesize()*8;
    void* block=heap_init(heapSize);
    void* mal=_malloc(100);
    struct block_header* header = block_get_header(mal);
    if (block==NULL || mal==NULL||header->is_free||header->capacity.bytes!=100) return false;
    debug_heap(stdout, block);
    _free(mal);
    munmap(HEAP_START, heapSize);
    return true;
}

bool test2(){
    size_t heapSize=getpagesize()*8;
    void* block=heap_init(heapSize);
    void* mal=_malloc(100);
    void* mal2= _malloc(100);
    if(!mal||!mal2)return false;
    struct block_header* addr=block_get_header(mal);
    struct block_header* addr2=block_get_header(mal2);
    if(addr->capacity.bytes!=100||addr2->capacity.bytes!=100)return false;
    debug_heap(stdout,block);
    _free(mal);
    if (!addr->is_free||addr2->is_free) return false;
    _free(mal2);
    munmap(HEAP_START, heapSize);
    return true;
}

bool test3(){
    size_t heapSize=getpagesize()*8;
    void* block=heap_init(heapSize);
    void* mal=_malloc(100);
    void* mal2= _malloc(100);
    void* mal3= _malloc(100);
    if(!mal||!mal2||!mal3)return false;
    struct block_header* addr=block_get_header(mal);
    struct block_header* addr2=block_get_header(mal2);
    struct block_header* addr3=block_get_header(mal3);
    if(addr->capacity.bytes!=100||addr2->capacity.bytes!=100||addr3->capacity.bytes!=100)return false;
    debug_heap(stdout,block);
    _free(mal);
    _free(mal2);
    if (!(addr->is_free&&addr2->is_free)||addr3->is_free) return false;
    _free(mal3);
    munmap(HEAP_START, heapSize);
    return true;
}

bool test4(){
    size_t heapSize=getpagesize()*8;
    void* block=heap_init(heapSize);
    void* mal=_malloc(heapSize*2);
    struct block_header* addr=block_get_header(mal);
    if(addr!=block||addr->capacity.bytes!=heapSize*2)return false;
    debug_heap(stdout,block);
    _free(mal);
    munmap(HEAP_START, heapSize);
    return true;
}

bool test5(){
    size_t heapSize=getpagesize()*8;
    void* block=heap_init(heapSize);
    void* block2=mmap((block+heapSize), heapSize, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED , -1, 0);
    if (block2==MAP_FAILED) return false;
    void* mal=_malloc(100);
    struct block_header* addr=block_get_header(mal);
    if(addr!=block||addr->capacity.bytes!=100||addr->is_free)return false;
    debug_heap(stdout,block);
    munmap(HEAP_START, heapSize*2);
    return true;
}

int main(){
    if(test1())printf("cool test1");
    else printf("bad test1");
    if(test2())printf("cool test2");
    else printf("bad test2");
    if(test3())printf("cool test3");
    else printf("bad test3");
    if(test4())printf("cool test4");
    else printf("bad test4");
    if(test5())printf("cool test5");
    else printf("bad test5");
    return 0;
}

